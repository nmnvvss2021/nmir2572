package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tasks.utils.ArrayTaskList;
import tasks.model.Task;
import tasks.utils.DateService;

import java.util.ArrayList;
import java.util.Date;

public class TasksService {

    private ArrayTaskList tasks;

    public TasksService(ArrayTaskList tasks){
        this.tasks = tasks;
    }

    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }

    public int GetTaskListSize() {
        return tasks.size();
    }

    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }

    public void add(Task task){
        tasks.add(task);
    }
    public String formTimeUnit(int timeUnit){
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }

    public int parseFromStringToSeconds(String stringTime){//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);
        return (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;
    }

    public Iterable<Task> filterTasks(Date start, Date end) throws Exception {
        ArrayList<Task> incomingTasks = new ArrayList<>();
        if (end.before(start)) {
            throw new IllegalArgumentException("End time cannot be before start time!");
        }
        else {
            TasksOperations tasksOperations = new TasksOperations(getObservableList());
            int i = 0;
            while (i < tasksOperations.tasks.size()) {
                Date nextTime = tasksOperations.tasks.get(i).nextTimeAfter(start);
                if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
                    incomingTasks.add(tasksOperations.tasks.get(i));
                }
                i++;
            }
            if (incomingTasks.isEmpty()) {
                //throw new Exception("No result found!");
            }
        }
        return incomingTasks;
    }
}
