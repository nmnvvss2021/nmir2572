package tasks.services;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import tasks.model.Task;
import tasks.utils.ArrayTaskList;

import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TasksServiceTestMock {
    @Mock
    ArrayTaskList arrayTaskList;
    @InjectMocks
    TasksService tasksService;

    @BeforeEach
    void setUp() {
        arrayTaskList = mock(ArrayTaskList.class);
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void addTask() {


        Task task = new Task("Titlu",new Date(1),new Date(2),1);

        Mockito.doNothing().when(arrayTaskList).add(task);

        tasksService.add(task);


        Mockito.verify(arrayTaskList,times(1)).add(task);


    }

    @Test
    void getAll() {
        when(arrayTaskList.getAll()).thenReturn(FXCollections.observableArrayList());
        tasksService.getObservableList();
        Mockito.verify(arrayTaskList,times(1)).getAll();
    }
}