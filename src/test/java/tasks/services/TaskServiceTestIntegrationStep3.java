package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.Task;
import tasks.utils.ArrayTaskList;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TasksServiceTestIntegrationStep3 {
    @Mock
    ArrayTaskList arrayTaskList;
    @InjectMocks
    TasksService tasksService;

    @BeforeEach
    void setUp() {
        arrayTaskList = mock(ArrayTaskList.class);
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void getObservableList() {

        Task task = new Task("Title1", new Date(100));

        Mockito.doNothing().when(arrayTaskList).add(task);

        tasksService.add(task);

        when(arrayTaskList.getAll()).thenReturn(FXCollections.observableArrayList());

        ObservableList<Task> observableList = tasksService.getObservableList();

        Mockito.verify(arrayTaskList,times(1)).add(task);
        Mockito.verify(arrayTaskList,times(1)).getAll();

    }

    @Test
    void filterTasks() throws Exception {

        Task task1 = new Task("Title1", new Date(100), new Date(200), 1);
        task1.setActive(true);
        Task task2 = new Task("Title2", new Date(200));
        Task task3 = new Task("Title3", new Date(300));

        Mockito.doNothing().when(arrayTaskList).add(task1);
        Mockito.doNothing().when(arrayTaskList).add(task2);
        Mockito.doNothing().when(arrayTaskList).add(task3);


        tasksService.add(task1);
        tasksService.add(task2);
        tasksService.add(task3);

//        when(arrayTaskList.getAll()).thenReturn(FXCollections.observableArrayList());
        ArrayList<Task> tasks = (ArrayList<Task>) tasksService.filterTasks(new Date(1), new Date(200));

        Mockito.verify(arrayTaskList,times(1)).add(task1);
        Mockito.verify(arrayTaskList,times(1)).add(task2);
        Mockito.verify(arrayTaskList,times(1)).add(task3);
//        Mockito.verify(arrayTaskList,times(1)).getAll();
    }
}