package tasks.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import tasks.utils.ArrayTaskList;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TasksServiceTest {
    TasksService tasksService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void filterTask_endTimeBeforeStartTime_exceptionExpected() {
        Task task = new Task("Task1",new Date(1000));

        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);
        tasksService = new TasksService(arrayTaskList);
        try{
            Iterable<Task> aux = null;
            aux =  tasksService.filterTasks(new Date(1000),new Date(900));
            assert(aux !=null);
        } catch (Exception e) {
            assert(e.getMessage().equals("End time cannot be before start time!"));
        }

    }

    @Test
    void filterTask_dateNotBetweenStartAndEnd_exceptionExpected(){
        Task task = new Task("Task1", new Date(500));
        task.setActive(true);
        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);
        tasksService = new TasksService(arrayTaskList);

        try {
            ArrayList<Task> taskIterable = (ArrayList<Task>) tasksService.filterTasks(new Date(1000), new Date(2000));
            assert (taskIterable.size() == 0);
        }
        catch (Exception e) {
            assert(e.getMessage().equals("No result found!"));
        }
    }

    @Test
    void filterTask_dateBetweenStartAndEnd_resultExpected() {
        Task task = new Task("Task1", new Date(1500));
        task.setActive(true);
        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);
        tasksService = new TasksService(arrayTaskList);

        try {
            ArrayList<Task> taskIterable = (ArrayList<Task>) tasksService.filterTasks(new Date(1000), new Date(2000));
            assert (taskIterable.size() == 1);
        }
        catch (Exception e) {
            assert(false);
        }
    }
    @Test
    void filterTask_dateEqualsEndDate_resultExpected() {
        Task task = new Task("Task1", new Date(2000));
        task.setActive(true);
        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task);
        tasksService = new TasksService(arrayTaskList);

        try {
            ArrayList<Task> taskIterable = (ArrayList<Task>) tasksService.filterTasks(new Date(1000), new Date(2000));
            assert (taskIterable.size() == 1);
        }
        catch (Exception e) {
            assert(false);
        }
    }
    @Test
    void filterTask_2datesOnlyOneBetweenInterval_resultExpected() {
        Task task1 = new Task("Task1", new Date(500));
        task1.setActive(true);
        Task task2 = new Task("Task1", new Date(2000));
        task2.setActive(true);
        ArrayTaskList arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(task1);
        arrayTaskList.add(task2);
        tasksService = new TasksService(arrayTaskList);

        try {
            ArrayList<Task> taskIterable = (ArrayList<Task>) tasksService.filterTasks(new Date(1000), new Date(2000));
            assert (taskIterable.size() == 1);
        }
        catch (Exception e) {
            assert(false);
        }
    }

}