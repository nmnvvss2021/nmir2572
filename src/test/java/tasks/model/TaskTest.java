package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {


    @Test
    void getTitle() {
        Task  task = new Task("Titlu",new Date(1),new Date(2),1);
        assertEquals("Titlu",task.getTitle());
    }

    @Test
    void setTitle() {
        Task  task = new Task("Titlu",new Date(1),new Date(2),1);
        task.setTitle("Titlu nou");
        assertEquals("Titlu nou",task.getTitle());
    }
}