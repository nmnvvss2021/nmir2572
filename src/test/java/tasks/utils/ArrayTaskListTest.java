package tasks.utils;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Nested;
import tasks.model.Task;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTaskListTest {

    ArrayTaskList taskList;
    @BeforeEach
    void setUp() {
        taskList = new ArrayTaskList();
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * Teste pentru parametrii time si interval
     */




    //grouped
    @Nested

        @DisplayName("Test nonvalid pentru parametrul time ")
        @Test
        void add_ecp1_nonvalid() {
            try{
                taskList.add(new Task("Titlu",new Date(-1)));
                assert(false);
            }catch (Exception e){
                assert(true);
            }
        }

        @Disabled
        @Test
        void add_ecp2_valid(){
            try{
                taskList.add(new Task("Titlu",new Date(1)));
                assert(true);
            }catch (Exception e){
                assert(false);
            }
        }

        @Timeout(1000)
        @Test
        void add_ecp3_nonvalid(){
            try{
                taskList.add(new Task("", new Date(1),new Date(2),-5));
                assert(false);
            }catch (Exception e){
                assert(true);
            }
        }

        @RepeatedTest(5)
        @Test
        void add_ecp4_valid(){
            try{
                taskList.add(new Task("Titlu", new Date(1),new Date(2),10));
                assert(true);
            }catch (Exception e){
                assert(false);
            }
        }
        @Test
        void add_ecp5_nonvalid(){
            try{
                taskList.add(new Task("", new Date(10)));
                assert(false);
            }catch (Exception e){
                assert(true);
            }
        }

    /**
     * add_ecp6_nonvalid() - pica testul deoarece in clasa Task in constructor nu se verifica daca lungimea titlul este zero
     */
    @Test
    void add_ecp6_nonvalid(){
        try{
            taskList.add(new Task("", new Date(1)));
            assert(false);
        }catch (Exception e){
            assert(true);
        }
    }


        @Test
        void add_bva1_nonvalid(){
            try{
                taskList.add(new Task("Titlu",new Date(1),new Date(2),0));
                assert(false);
            }catch (Exception e){
                assert(true);
            }
        }
    @Test
    void add_bva2_valid(){
        try{
            taskList.add(new Task("Titlu2",new Date(1),new Date(2),1));
            assert(true);
        }catch (Exception e){
            assert(false);
        }
    }
    @Test
    void add_bva3_valid(){
        try{
            taskList.add(new Task("Titlu3",new Date(1),new Date(2),2));
            assert(true);
        }catch (Exception e){
            assert(false);
        }
    }

    @Test
    void add_bva4_nonvalid(){
        try{
            taskList.add(new Task("Titlu4",new Date(-1)));
            assert(false);
        }catch (Exception e){
            assert(true);
        }
    }
    @Test
    void add_bva5_valid(){
        try{
            taskList.add(new Task("Titlu5",new Date(0)));
            assert(true);
        }catch (Exception e){
            assert(false);
        }
    }
    @Test
    void add_bva6_valid(){
        try{
            taskList.add(new Task("Titlu6",new Date(1)));
            assert(true);
        }catch (Exception e){
            assert(false);
        }
    }



}