package tasks.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import tasks.model.Task;
import tasks.services.TasksService;
import tasks.utils.ArrayTaskList;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ArrayTaskListTestMock {


    private ArrayTaskList arrayTaskList;

    @BeforeEach
    void setUp() {
        arrayTaskList = new ArrayTaskList();
    }

    @Test
    void add() {

        Task task = mock(Task.class);


        arrayTaskList.add(task);


        assert(arrayTaskList.size() == 1);
    }

    @Test
    void remove() {

        Task task1 = mock(Task.class);
        Task task2 = mock(Task.class);


        arrayTaskList.add(task1);
        arrayTaskList.add(task2);
        arrayTaskList.remove(task1);


        assert(arrayTaskList.size() == 1);
    }

    @Test
    void size() {

        Task task1 = mock(Task.class);


        arrayTaskList.add(task1);
        arrayTaskList.remove(task1);


        assert(arrayTaskList.size() == 0);
    }

}